import { CreateProjectInput } from './create-project.input';
import { InputType, Field, Int, PartialType } from '@nestjs/graphql';

@InputType()
export class UpdateProjectInput {
  @Field()
  name: string

  @Field(() => Int)
  code: number
}
// extends PartialType(CreateProjectInput) 