import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import { CreateProjectInput } from './dto/create-project.input';
import { UpdateProjectInput } from './dto/update-project.input';
import { Project } from './entities/project.entity';

@Injectable()
export class ProjectService {
  constructor(
    @InjectRepository(Project)
    private readonly projectRepository: Repository<Project>
  ) {}
  
  async create(createProjectInput: CreateProjectInput): Promise<Project> {
    return await this.projectRepository.save(createProjectInput);
  }

  async findAll(): Promise<Project[]> {
    return await this.projectRepository.find({ relations: ['employees'] });
  }

  async findOne(id: string): Promise<Project> {
    return await this.projectRepository.findOne(id, { relations: ['employees'] });
  }

  async update(id: string, updateProjectInput: UpdateProjectInput): Promise<any> {
    const project = await this.projectRepository.findOne(id);
    if (project) {
      return await this.projectRepository.save({ id, ...updateProjectInput });
    }
    throw new NotFoundException(`Error updating project with ID ${id}.`);
  }

  async remove(id: string): Promise<any> {
    const project = await this.projectRepository.findOne(id);
    if (project) {
      const result = await this.projectRepository.delete(id);
      if (result.affected > 0) {
        return `Record with ID ${id} deleted successfully.`
      }
    }
    throw new NotFoundException(`Error deleting project with ID ${id}.`)
  }
}
