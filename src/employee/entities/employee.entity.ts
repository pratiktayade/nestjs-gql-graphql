import { Field, ID, ObjectType } from "@nestjs/graphql"
import { Project } from "src/project/entities/project.entity"
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm"

@ObjectType()
@Entity()
export class Employee {
  @Field(type => ID)
  @PrimaryGeneratedColumn('uuid')
  id: string

  @Field()
  @Column()
  firstName: string

  @Field()
  @Column()
  lastName: string
  
  @Field()
  @Column()
  designation: string
  
  @Field()
  @Column()
  city: string

  // One project has many employees
  @ManyToOne(() => Project, project => project.employees)
  @Field(() => Project)
  project: Project

  @Field()
  @Column()
  projectId: string
}