import { Field, ID, InputType } from "@nestjs/graphql"

@InputType()
export class CreateEmployeeDTO {
  @Field()
  firstName: string

  @Field()
  lastName: string
  
  @Field()
  designation: string
  
  @Field()
  city: string

  @Field()
  projectId: string
}