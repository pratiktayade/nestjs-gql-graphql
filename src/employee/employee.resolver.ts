import { Args, Mutation, Parent, Query, ResolveField, Resolver } from '@nestjs/graphql';
import { Project } from 'src/project/entities/project.entity';
import { ProjectService } from 'src/project/project.service';
import { CreateEmployeeDTO } from './dto/create-employee.input';
import { EmployeeService } from './employee.service';
import { Employee } from './entities/employee.entity';

@Resolver(() => Employee)
export class EmployeeResolver {
  constructor(
    private employeeService: EmployeeService,
    private projectService: ProjectService
  ) {}

  @Query(() => [Employee], { name: 'getAllEmployees' })
  async findAll(): Promise<Employee[]> {
    return this.employeeService.findAll();
  }

  @Query(() => Employee, { name: 'employee' })
  findOne(@Args('id') id: string) {
    return this.employeeService.findOne(id);
  }

  @Mutation(() => Employee, { name: 'createEmployee' })
  async create(@Args('employee') employee: CreateEmployeeDTO): Promise<Employee> {
    return this.employeeService.create(employee);
  }

  @ResolveField(() => Project)
  async project(@Parent() employee: Employee): Promise<Project> {
    return await this.projectService.findOne(employee.projectId);
  }
}
